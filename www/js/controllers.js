angular.module('polynews.controllers', [])

.controller('HomeCtrl', function($scope,Articles) {
    $scope.articles = Articles.all();
    $scope.alauneID = Articles.alaune();
  })

.controller('PolitiquesCtrl', function($scope, Articles) {
  $scope.politiques = Articles.all();
  $scope.remove = function(politique) {
    Articles.remove(politique);
  };
})

.controller('ArticleDetailCtrl', function($scope, $stateParams, Articles) {
    $scope.politique = Articles.get($stateParams.articleID);
})

.controller('SocietesCtrl', function($scope, Articles) {
  $scope.societes = Articles.all();
});
