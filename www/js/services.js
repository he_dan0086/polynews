angular.module('polynews.services', [])

.factory('Articles', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var articles = [{
    id: 0,
    title: 'Copé-Sarkozy : « Il y aura un mort à la fin »',
    author: 'Author0',
    content: 'La charge est lourde. Alors qu’il avait toujours dédouané Nicolas Sarkozy, Jérôme Lavrilleux a mis gravement en cause l’ancien chef de l’Etat dans l’affaire Bygmalion. Dans un entretien publié par L’Obs mardi 13 octobre, l’ex-directeur de cabinet de Jean-François Copé l’accuse de « se défausser » et de ne pas « assumer » ses responsabilités dans un système de fausses factures durant sa campagne présidentielle de 2012. Pas de doute pour le député européen : le président des Républicains (LR) était au courant. « Les comptes ont débordé de tous les côtés. Il n’y a que Nicolas Sarkozy pour dire dans sa déposition que cette affaire ne concerne pas sa campagne… C’est un système de défense voué à un échec total. » « Je n’attache aucune importance et aucune crédibilité à ces propos », a réagi Nicolas Sarkozy mercredi soir sur BFM-TV, avant d’ajouter : « La justice est saisie, eh bien elle dira. »' +
    'La sortie médiatique de M. Lavrilleux n’est pas anodine. Elle s’inscrit dans un contexte de tension extrême régnant entre Jean-François Copé et Nicolas Sarkozy. Depuis une entrevue en juillet, les deux hommes ne se sont pas adressé la parole. Le fidèle sarkozyste Brice Hortefeux, qui a déjeuné avec M. Copé mercredi, joue le rôle d’intermédiaire. Les relations entre MM. Copé et Sarkozy se sont envenimées ces dernières semaines, à cause des développements judiciaires de l’affaire Bygmalion.' +
    '« Il se défausse, il vit dans un monde irréel et ne sait pas assumer. Les grands chefs sont pourtant ceux qui assument »' +
    'Alors que le',
    video: 'https://www.youtube.com/embed/LJAFrtsgO9M?list=PLFuK0VAIne9J5GCVktyvJH6gAyF1AeXhR',
    images:['http://img.youtube.com/vi/LJAFrtsgO9M/default.jpg'],
    alaune: 1,
    date: '09/10/2015',
    category: 'politiques'
  }, {
    id: 1,
    title: 'title1',
    author: 'Author1',
    content: 'Former des ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales et la ' +
    'maîtrise des techniques les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité',
    images:['img/1.jpg','img/alaune.jpg'],
    alaune: 0,
    date: '09/10/2015',
    category: 'politiques'
  }, {
    id: 2,
    title: 'title2',
    author: 'Author2',
    content: 'Former des ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales et ' +
    'la maîtrise des techniques les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité',
    video: 'https://www.youtube.com/embed/LJAFrtsgO9M?list=PLFuK0VAIne9J5GCVktyvJH6gAyF1AeXhR',
    images:['http://img.youtube.com/vi/LJAFrtsgO9M/default.jpg'],
    alaune: 0,
    date: '09/10/2015',
    category: 'politiques'
  }, {
    id: 3,
    title: 'title3',
    author: 'Author3',
    content: 'Former des ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales et ' +
    'la maîtrise des techniques les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité, ' +
    '\n Former des ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales et la maîtrise des ' +
    'techniques les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité, \nFormer des ' +
    'ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales et la maîtrise des techniques ' +
    'les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité, Former des ingénieurs ' +
    'en Informatique qui possèdent à la fois les connaissances fondamentales et la maîtrise des techniques les plus ' +
    'avancées de ce domaine et les préparer à accéder à des postes à responsabilité',
    images:['img/alaune.jpg','img/alaune.jpg','img/alaune.jpg'],
    alaune: 0,
    date: '09/10/2015',
    category: 'politiques'
  }, {
    id: 4,
    title: 'title4',
    author: 'Author4',
    content: 'Former des ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales et la' +
    ' maîtrise des techniques les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité',
    images:['img/alaune.jpg','img/alaune.jpg','img/alaune.jpg'],
    alaune: 0,
    date: '09/10/2015',
    category: 'societes'
  }, {
    id: 5,
    title: 'title5',
    author: 'Author5',
    content: 'Former des ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales et ' +
    'la maîtrise des techniques les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité',
    images:['img/alaune.jpg','img/alaune.jpg','img/alaune.jpg'],
    alaune: 0,
    date: '09/10/2015',
    category: 'societes'
  }, {
    id: 6,
    title: 'title6',
    author: 'Author6',
    content: 'Former des ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales ' +
    'et la maîtrise des techniques les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité',
    video: 'https://www.youtube.com/embed/LJAFrtsgO9M?list=PLFuK0VAIne9J5GCVktyvJH6gAyF1AeXhR',
    images:['http://img.youtube.com/vi/LJAFrtsgO9M/default.jpg'],
    alaune: 0,
    date: '09/10/2015',
    category: 'societes'
  }, {
    id: 7,
    title: 'title7',
    author: 'Author7',
    content: 'Former des ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales et ' +
    'la maîtrise des techniques les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité',
    images:['img/alaune.jpg','img/alaune.jpg','img/alaune.jpg'],
    alaune: 1,
    date: '09/10/2015',
    category: 'societes'
  }, {
    id: 8,
    title: 'title8',
    author: 'Author8',
    content: 'Former des ingénieurs en Informatique qui possèdent à la fois les connaissances fondamentales et l' +
    'a maîtrise des techniques les plus avancées de ce domaine et les préparer à accéder à des postes à responsabilité',
    images:['img/alaune.jpg','img/alaune.jpg','img/alaune.jpg'],
    alaune: 0,
    date: '09/10/2015',
    category: 'politiques'
  }];

    var hasALaUne=false;
  return {
    all: function() {
      return articles;
    },

    remove: function(article) {
      articles.splice(articles.indexOf(article), 1);
    },
    get: function(articleID) {
      for (var i = 0; i < articles.length; i++) {
        if (articles[i].id === parseInt(articleID)) {
          return articles[i];
        }
      }
      return null;
    },
    alaune : function(){
      for (var i = 0; i < articles.length; i++) {
        if (articles[i].alaune ==1) {
          return articles[i].id;
        }
      }
    }
  };
});
